package com.myntra.migration.snapshortmigration.domain;

import lombok.Data;

/**
 * @author Abhishyam.c on 01/12/20
 */
@Data
public class TruncateData {
    private String schema;

}
