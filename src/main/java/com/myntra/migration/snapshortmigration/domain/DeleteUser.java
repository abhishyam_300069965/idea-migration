package com.myntra.migration.snapshortmigration.domain;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Abhishyam.c on 15/04/21
 */
@Data
public class DeleteUser {
    private List<BigDecimal> ids;
    private String tenant;
}
