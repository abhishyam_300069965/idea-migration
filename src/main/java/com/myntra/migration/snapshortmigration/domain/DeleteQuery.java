package com.myntra.migration.snapshortmigration.domain;

import lombok.Data;

import java.util.List;

/**
 * @author Abhishyam.c on 27/11/20
 */
@Data
public class DeleteQuery {
    private List<String> queriesToDelete;
    private String dbName;
}
