package com.myntra.migration.snapshortmigration.domain;

import lombok.Data;

import java.util.List;

/**
 * @author Abhishyam.c on 01/04/21
 */
@Data
public class TenantUpdateRequest {
    private Integer partnerId;
    private Integer tenantId;
}
