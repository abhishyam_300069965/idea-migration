package com.myntra.migration.snapshortmigration.domain;

import lombok.Data;

/**
 * @author Abhishyam.c on 31/05/21
 */
@Data
public class FlipkartUser {
    private Long uid;
    private String uidx;
}
