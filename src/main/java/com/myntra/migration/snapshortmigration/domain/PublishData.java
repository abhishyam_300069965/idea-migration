package com.myntra.migration.snapshortmigration.domain;

import lombok.Data;

import java.math.BigInteger;
import java.util.List;

/**
 * @author Abhishyam.c on 02/11/20
 */
@Data
public class PublishData {
    private Long start;
    private Long end;
    private String uidx;
    private BigInteger id;
    private String filePath;
    private String schema;
    private long sleep;
    private int recordsCountToPause;
    private boolean migrateAll = false;
    private String clientId;
    private Integer tenantId;
    private String columnName;
    private Object columnValue;
    private String eventName;
    private List<String> users;
}