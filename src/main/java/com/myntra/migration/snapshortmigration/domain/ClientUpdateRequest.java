package com.myntra.migration.snapshortmigration.domain;

import lombok.Data;

import java.util.List;

/**
 * @author Abhishyam.c on 01/04/21
 */
@Data
public class ClientUpdateRequest {
    private Integer clientId;
    List<String> clientNames;
    private Long tenantId;
    private String columnName;
    private Object columnValues;
}