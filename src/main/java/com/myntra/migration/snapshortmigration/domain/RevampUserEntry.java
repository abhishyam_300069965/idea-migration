package com.myntra.migration.snapshortmigration.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RevampUserEntry {
    private String status;
    private Long dob;
    private Boolean verified = false;
    private String channel;
    private String email;
    private String normalizedEmail;
    private Boolean emailVerified = false;
    private String phone;
    private Boolean phoneVerified = false;
    private Long phoneVerifiedOn;
    private String username;
    private Long id;
    private String alternatePhone;
    private String amnNickName;
    private String firstName;
    private String lastName;
    private String gender;
    private Long registrationOn;
    private String userType;
    private String uidx;
    private boolean new_;
    private String publicProfileId;
    private String location;
    private String bio;
    private String extraData;
    private String isdCode;
    private String schema;
}
