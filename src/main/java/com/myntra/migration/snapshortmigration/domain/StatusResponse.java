package com.myntra.migration.snapshortmigration.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StatusResponse {
    private long statusCode;
    private String message;
}
