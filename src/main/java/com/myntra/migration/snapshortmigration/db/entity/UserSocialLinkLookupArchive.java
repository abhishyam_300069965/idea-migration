package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

/**
 * @author Abhishyam.c on 05/11/20
 */
@Data
@Table("user_social_link_lookup_archive")
public class UserSocialLinkLookupArchive {
    @Id
    protected Long id;
    private String uidx;
    private String sid;
    private Integer type;
    private Long uid;
    private Long appid;
    protected Timestamp createdOn;
    protected String createdBy;
    protected Timestamp lastModifiedOn;
    private Timestamp archivedOn;
    protected Long version = 0L;
}
