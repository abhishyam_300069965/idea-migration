package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author yogeshpandey
 *
 */
@Data
@Table("user_social_link_lookup")
public class UserSocialLookup {

	@Id
	protected Long id;
	private Long uid;
	private Long appid;
	private String uidx;
	private String sid;
	private int type;
	private boolean deleted;
	protected String createdBy;
	protected LocalDateTime createdOn;
	protected LocalDateTime lastModifiedOn;
	protected Long version = 0L;
}
