package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigInteger;

/**
 * @author Anshuman
 */
@Data
public class UserRecoveryRequest{

    @Id
    private BigInteger id;
    private String uidx;
    private String email;
    private String resetKey;
    private Integer type;
    private Integer status;
    private Long endsOn;
    private String extraData;
}
