package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class PasswordPolicy {
    @Id
    private Long id;
    private Integer validity;
    private Integer minLength;
    private Integer maxLength;
    private Long constraints;
    private Long recovery;
    private Integer passwordRecycleLimit;
    private Long hashAlgo;
    private Integer saltLength;
    private Integer resetRequestValidity;
    private String regex;
    private Integer strength;
    private String info;
    private Integer notSameAsPreviousCredentialCount;
    private Integer maxFailedAttempts;
    private Integer autoUnlockDuration;
    private Long version;
    private Long createdOn;
    private Long lastModifiedOn;
}
