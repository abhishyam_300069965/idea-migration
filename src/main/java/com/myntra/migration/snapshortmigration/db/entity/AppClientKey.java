package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;


@Table("client_keys")
@Data
public class AppClientKey {
	@Id
	protected Long id;
	protected Long clientid;
	protected String keyAlgo;
	protected String keyType;
	protected String priKey;
	protected String pubKey;
	protected Timestamp startsOn;
	protected Timestamp endsOn;
	protected Long version = 0L;
	protected Integer active;
	protected String createdBy;
	protected Timestamp createdOn;
	protected Timestamp lastModifiedOn;
	protected int keyVersion;
}