package com.myntra.migration.snapshortmigration.db.connections;

import com.myntra.migration.snapshortmigration.vault.VaultUtil;
import dev.miku.r2dbc.mysql.MySqlConnectionConfiguration;
import dev.miku.r2dbc.mysql.MySqlConnectionFactory;
import io.r2dbc.pool.ConnectionPool;
import io.r2dbc.pool.ConnectionPoolConfiguration;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.r2dbc.core.DatabaseClient;
import org.springframework.data.r2dbc.core.DefaultReactiveDataAccessStrategy;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.r2dbc.dialect.MySqlDialect;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.Duration;

/**
 * @author Abhishyam.c on 04/11/20
 */
@Component
@Getter
@Slf4j
public class MasterDbConnection {

    @Value("${accountservice.dbs.master.username}")
    private String username;
    @Value("${accountservice.dbs.master.password}")
    private String password;
    @Value("${accountservice.dbs.master.service}")
    private String service;
    @Value("${accountservice.dbs.master.credential}")
    private String credential;
    @Value("${accountservice.dbs.master.host}")
    private String host;
    @Value("${accountservice.dbs.master.name}")
    private String name;
    @Value("${accountservice.dbs.master.port}")
    private int port;

    private R2dbcEntityTemplate r2dbcEntityTemplate;

    @PostConstruct
    private void getMasterDBConnection() {
            String userName, pass;
        if (StringUtils.isNotEmpty(service) && StringUtils.isNotEmpty(credential)) {
            JSONObject credentials = VaultUtil.getCredentials(service, credential);
            userName = credentials.getString("credential_id");
            pass = credentials.getString("credential_secret");
        } else {
            userName = username;
            pass  = password;
        }
        log.info("name: {}", name);
        log.info("port: {}", port);
            MySqlConnectionFactory mySqlConnectionFactory = MySqlConnectionFactory.from(MySqlConnectionConfiguration.builder()
                    .host(host)
                    .port(port)
                    .database(name)
                    .username(userName)
                    .password(pass)
                    .build());

        ConnectionPoolConfiguration configuration = ConnectionPoolConfiguration.builder(mySqlConnectionFactory)
                .maxIdleTime(Duration.ofMinutes(30))
                .initialSize(1)
                .maxSize(5)
                .maxCreateConnectionTime(Duration.ofSeconds(10))
                .build();
        DatabaseClient databaseClient = DatabaseClient.create(new ConnectionPool(configuration));

        DefaultReactiveDataAccessStrategy strategy = new DefaultReactiveDataAccessStrategy(MySqlDialect.INSTANCE);
        r2dbcEntityTemplate = new R2dbcEntityTemplate(databaseClient, strategy);
    }

}
