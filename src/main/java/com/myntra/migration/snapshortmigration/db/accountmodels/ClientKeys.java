package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Table("client_keys")
@Data
public class ClientKeys {
    @Id
    private Integer id;
    private Integer clientId;
    private int keyType;
    private int keyAlgo;
    private String privateKey;
    private String publicKey;
    private boolean active;
    private long startsOn;
    private Long endsOn;
    protected int keyVersion;
    private Long createdOn;
    private Long lastModifiedOn;
    private Long version;
}
