package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.UserEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Abhishyam.c on 02/11/20
 */

public interface UserEntityRepo extends ReactiveCrudRepository<UserEntity, Long> {

     Mono<UserEntity> findById(Long id);
     Flux<UserEntity> findByIdGreaterThanEqualAndIdLessThanEqualAndAppIdEquals(Long start, Long end, Long appId);
}
