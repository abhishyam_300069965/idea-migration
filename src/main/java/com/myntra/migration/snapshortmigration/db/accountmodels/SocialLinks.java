package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigInteger;

@Data
public class SocialLinks{
    @Id
    private BigInteger id;
    private BigInteger uid;
    private String sid;
    private Integer type;
}
