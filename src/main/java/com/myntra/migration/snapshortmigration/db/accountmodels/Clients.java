package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
public class Clients {
    @Id
    private Integer id;
    private Long tenantId;
    protected String name;
    private Integer active;
    private boolean deleted;
    private Long version;
    private Integer status;
    private String cidx;
    private String clientSecret;
    private String grantTypes;
    private String scope;
    protected String type;
    private boolean checkPasswordLifetime;
    private boolean isSessionMappingEnabled;
    private String loginOption;
    private boolean embedScopeData;
    private Long registrationOn;
    private Long expiryOn;
    private Long createdOn;
    private Long lastModifiedOn;
    private String rateLimit;
    private Integer intent;
}
