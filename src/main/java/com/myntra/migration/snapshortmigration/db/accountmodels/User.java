package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigInteger;

@Data
public class User {
    @Id
    private BigInteger id;
    private String uidx;
    private String firstName;
    private String lastName;
    private Integer gender;
    private Long dob;
    private String bio;
    private String email;
    private String normalizedEmail;
    private Boolean emailVerified;
    private String isdCode;
    private String phone;
    private Boolean phoneVerified;
    private Integer status;
    private Integer userType;
    private Integer channel;
    private String amnIsdCode;
    private String alternatePhone;
    private String amnNickName;
    private String extraData;
    private String location;
    private String publicProfileId;
    private Long phoneVerifiedOn;
    private Long emailVerifiedOn;
    private Long registrationOn;
}