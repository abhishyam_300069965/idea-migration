package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.AppClientKey;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * @author Abhishyam.c on 04/11/20
 */
public interface AppClientKeysRepo extends ReactiveCrudRepository<AppClientKey, Long> {
    Flux<AppClientKey> findByClientid(Long clientId);
}
