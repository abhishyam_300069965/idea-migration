package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author Abhishyam.c on 05/11/20
 */
@Data
public class UserSocialLinkArchive {
    @Id
    protected Long id;
    private String uidx;
    private String sid;
    private Integer type;
    private Long uid;
    protected Long createdOn;
    protected Long lastModifiedOn;
    private LocalDateTime archivedOn;
    protected Long version = 0L;
}
