package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 * @author yogeshpandey
 *
 */
@Data
@Table("user_email_lookup")
public class UserEmailLookup {
	@Id
	protected Long id;
	private Long uid;
	private Long appid;
	private String uidx;
	private String email;
	private String normalizedEmail;
	private boolean verified;
	private boolean deleted;
}
