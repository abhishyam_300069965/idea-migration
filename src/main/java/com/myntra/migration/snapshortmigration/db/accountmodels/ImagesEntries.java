package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.math.BigInteger;


@Data
public class ImagesEntries{
    @Id
    private BigInteger id;
    private BigDecimal uid;
    private BigInteger imageId;
    private BigInteger coverImageId;
    private String imageUrl;
    private String coverImageUrl;
    private Integer imageType;
    private Integer coverImageType;
    private String jsonEntry;
}
