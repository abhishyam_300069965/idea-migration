package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

/**
 * @author Abhishyam.c on 05/11/20
 */
@Data
@Table("user_email_lookup_archive")
public class UserEmailLookupArchive {
    @Id
    protected Long id;
    private String uidx;
    private String email;
    private Long uid;
    private Long appid;
    protected Timestamp createdOn;
    protected String createdBy;
    protected Long version = 0L;
    protected Timestamp lastModifiedOn;
    private boolean verified;
    private String normalizedEmail;
    private Timestamp archivedOn;
}
