package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author Abhishyam.c on 05/11/20
 */
@Data
public class UserPhoneArchive {
    @Id
    protected Long id;
    private Long uid;
    private String uidx;
    private String phone;
    private boolean verified;
    protected Long createdOn;
    protected Long version = 0L;
    protected Long lastModifiedOn;
    private String alternatePhone;
    private LocalDateTime archivedOn;
    private Long verifiedOn;
}
