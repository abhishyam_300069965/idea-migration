package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
public class ClientsProperties {
    @Id
    private Integer id;
    private Integer clientId;
    private Integer atValidity;
    private Integer rtValidity;
    private String redirectUri;
    private boolean encodeToken;
    private boolean issueRt;
    private int refreshTokenType;
    private Integer rtInactivity;
    private Integer maxConcurrentSessions;
    private boolean concurrentLoginEnabled;
    private Integer apiUserAtValidity;
}