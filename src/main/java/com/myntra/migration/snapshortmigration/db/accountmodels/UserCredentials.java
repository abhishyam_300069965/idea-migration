package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.math.BigInteger;

@Data
public class UserCredentials {
    @Id
    private BigInteger id;
    private BigInteger uid;
    private String password;
    private int failedLoginAttempts;
    private boolean forceChange;
    private String oldCredentials;
    private Long endsOn;
    private Long lastFailedOn;
    private Long passwordLastChangedOn;
}
