package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.UserPhoneLookupArchive;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * @author Abhishyam.c on 05/11/20
 */
public interface UserPhoneLookupArchiveRepo extends ReactiveCrudRepository<UserPhoneLookupArchive, Long> {
    Flux<UserPhoneLookupArchive> findByIdGreaterThanEqualAndIdLessThanEqual(Long start, Long end);
}
