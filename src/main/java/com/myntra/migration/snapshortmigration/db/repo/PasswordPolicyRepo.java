package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.PasswordPolicy;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * @author Abhishyam.c on 04/11/20
 */
public interface PasswordPolicyRepo extends ReactiveCrudRepository<PasswordPolicy, Long> {
}
