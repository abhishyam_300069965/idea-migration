package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;


@Data
public class Tenants {
    @Id
    private Integer id;
    private Long passId;
    private String name;
    private String domain;
    private Integer active;
    private String description;
    private String ssoDomain;
    private Integer storeId;
    private Long startsOn;
    private Long endsOn;
    private String createdBy;
    private String datasourceName;
    private Integer version;
    private Long createdOn;
    private Long lastModifiedOn;
}
