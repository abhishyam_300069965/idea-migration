package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.UserEmailDetail;
import com.myntra.migration.snapshortmigration.db.entity.UserEmailLookup;
import com.myntra.migration.snapshortmigration.db.entity.UserEmailLookupArchive;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * @author Abhishyam.c on 05/11/20
 */
public interface UserEmailLookupRepo extends ReactiveCrudRepository<UserEmailLookup, Long> {
    Flux<UserEmailLookup> findAllByEmailIn(List<String> emails);
}
