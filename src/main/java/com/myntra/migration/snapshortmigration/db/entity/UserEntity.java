package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * @author Abhishyam.c on 02/11/20
 */
@Data
@Table("user")
public class UserEntity {
    @Id
    @Column("id")
    private Long id;

    @Column("uidx")
    private String uidx;

    @Column("appid")
    private Long appId;
}
