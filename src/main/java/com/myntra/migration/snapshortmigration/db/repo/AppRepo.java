package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.App;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

/**
 * @author Abhishyam.c on 04/11/20
 */
public interface AppRepo extends ReactiveCrudRepository<App, Long> {
}
