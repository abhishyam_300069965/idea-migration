package com.myntra.migration.snapshortmigration.db.accountmodels;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

/**
 * @author Abhishyam.c on 05/11/20
 */
@Data
public class UserEmailArchive {
    @Id
    protected Long id;
    private String uidx;
    private String email;
    private Long uid;
    protected Long createdOn;
    protected Long version = 0L;
    protected Long lastModifiedOn;
    private boolean verified;
    private String normalizedEmail;
    private LocalDateTime archivedOn;
    private Long verifiedOn;
}
