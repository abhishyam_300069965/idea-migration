package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.UserSocialLinkLookupArchive;
import com.myntra.migration.snapshortmigration.db.entity.UserSocialLookup;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * @author Abhishyam.c on 05/11/20
 */
public interface UserSocialLinkLookupRepo extends ReactiveCrudRepository<UserSocialLookup,Long> {
    Flux<UserSocialLookup> findByUidx(String uidx);
}
