package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 * @author yogeshpandey
 *
 */
@Data
@Table("user_emails")
public class UserEmailDetail{

	@Id
	protected Long id;
	private Long uid;
	private String email;
	private boolean isPrimary;
}
