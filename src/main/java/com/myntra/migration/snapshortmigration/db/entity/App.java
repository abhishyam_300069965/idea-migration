package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.beans.Transient;
import java.sql.Timestamp;

/**
 * @author Abhishyam.c on 04/11/20
 */
@Data
@Table("apps")
public class App {
    @Id
    private Long id;
    private String name;
    private String domain;
    private Timestamp startsOn;
    private Timestamp endsOn;
    private Integer active;
    private String description;
    private Timestamp createdOn;
    private String createdBy;
    private Timestamp lastModifiedOn;
    private Long version;
    private String ssoDomain;
    private Integer storeId;
}
