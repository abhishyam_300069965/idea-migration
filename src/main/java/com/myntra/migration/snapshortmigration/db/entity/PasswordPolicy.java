package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

/**
 * @author Abhishyam.c on 04/11/20
 */
@Data
@Table("password_policy")
public class PasswordPolicy {
    @Id
    private Long id;
    private String name;
    private Integer validity;
    private Integer minLength;
    private Integer maxLength;
    private Integer constraints;
    private Integer recovery;
    private Integer passwordRecycleLimit;
    private Long hashAlgo;
    private Integer saltLength = 20;
    private Integer resetRequestValidity;
    private String regex;
    private Integer strength;
    private String info;
    private Integer notSameAsPreviousCredentialCount;
    private Integer maxFailedAttempts;
    private Integer autoUnlockDuration;
    private String createdBy;
    private Timestamp createdOn;
    private Timestamp lastModifiedOn;
    private Long version = 0L;
}
