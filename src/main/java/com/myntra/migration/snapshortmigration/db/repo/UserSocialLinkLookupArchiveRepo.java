package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.UserSocialLinkLookupArchive;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * @author Abhishyam.c on 05/11/20
 */
public interface UserSocialLinkLookupArchiveRepo extends ReactiveCrudRepository<UserSocialLinkLookupArchive,Long> {
    Flux<UserSocialLinkLookupArchive> findByIdGreaterThanEqualAndIdLessThanEqual(Long start, Long end);
}
