package com.myntra.migration.snapshortmigration.db.repo;

import com.myntra.migration.snapshortmigration.db.entity.UserEmailLookupArchive;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

/**
 * @author Abhishyam.c on 05/11/20
 */
public interface UserEmailLookupArchiveRepo extends ReactiveCrudRepository<UserEmailLookupArchive, Long> {
    Flux<UserEmailLookupArchive> findByIdGreaterThanEqualAndIdLessThanEqual(Long start, Long end);
}
