package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

/**
 * @author Abhishyam.c on 04/11/20
 */
@Table("app_password_policy")
@Data
public class AppPasswordPolicy {
    @Id
    private Long appid;
    private Long passwordPolicyid;
}
