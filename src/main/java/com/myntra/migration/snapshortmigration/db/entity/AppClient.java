package com.myntra.migration.snapshortmigration.db.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.sql.Timestamp;

@Table("clients")
@Data
public class AppClient{
	@Id
	protected Long id;
	protected String name;
	protected Timestamp registrationOn;
	protected Timestamp expiryOn;
	protected Long appid;
	protected Integer active;
	protected String createdBy;
	protected Timestamp createdOn;
	protected Timestamp lastModifiedOn;
	protected Long version = 0L;
	protected Boolean deleted;
	protected Integer status;
	protected String cidx;
	protected Integer rtValidity;
	protected Integer atValidity;
	protected Integer watValidity;
	protected String clientSecret;
	protected String grantTypes;
	protected String redirectUri;
	protected String scope;
	protected String type;
	protected Integer encodeToken;
	protected Integer issueRt;
	protected Integer refreshTokenType;
	protected Integer rtInactivity; // the unit of this is mninutes
	protected Integer partnerId;
	protected Integer checkPasswordLifetime; // not null
	protected Boolean isInactivityEnabled;
	protected Integer inactivityDays;
	protected Integer maxConcurrentSession;
	protected Boolean isBlockingConcurrentLoginEnabled;
	protected Long groupId;
	protected Integer groupRtValidity;
	protected boolean isSessionMappingEnabled;
	protected String backgroundImage;
	protected String logo;
	protected String loginOptions;
	protected String theme;
	protected String uidx;
	protected Integer apiUserATValidity;
	protected Boolean isUserSignupProtected;
	protected boolean embedScopeData;
}
