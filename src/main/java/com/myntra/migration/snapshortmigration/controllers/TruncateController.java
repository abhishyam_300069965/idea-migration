package com.myntra.migration.snapshortmigration.controllers;

import com.myntra.migration.snapshortmigration.domain.PublishData;
import com.myntra.migration.snapshortmigration.domain.RevampUserEntry;
import com.myntra.migration.snapshortmigration.service.TenantDbMigrationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * @author Abhishyam.c on 01/12/20
 */
@RestController
@Slf4j
public class TruncateController {
    @Autowired
    private TenantDbMigrationService tenantDbMigrationService;

    @DeleteMapping("recoveryRequest")
    public void deleteRecoveryRequest(@RequestBody PublishData publishData){
        tenantDbMigrationService.deleteRecoveryRequest(publishData);
    }

    @PostMapping("updateUser")
    public void updateUser(@RequestBody PublishData publishData){
        log.info("Updating the user table column {}", publishData.getColumnName());
        tenantDbMigrationService.updateUser(publishData);
    }

    @PostMapping("getDiff")
    public Map<Long, String> getDiffInUidx(@RequestBody PublishData publishData) throws IOException, URISyntaxException {
        log.info("Checking the Differences in uidx b/w Idea 1.0 to 2.0");
        return tenantDbMigrationService.getDifferences(publishData);
    }

    @PostMapping("getDiffReverse")
    public void getDiffReverse(@RequestBody PublishData publishData) throws IOException, URISyntaxException {
        log.info("Checking the Differences in uidx b/w Idea 2.0 to 1.0");
        tenantDbMigrationService.getDifferencesReverse(publishData);
    }

    @PostMapping("updateSocialLinks")
    public void updateSocial(@RequestBody PublishData publishData){
        log.info("Updating the social links table column {}", publishData.getColumnName());
        tenantDbMigrationService.updateSocialLinks(publishData);
    }
}
