package com.myntra.migration.snapshortmigration.controllers;

import com.myntra.migration.snapshortmigration.FileUtil;
import com.myntra.migration.snapshortmigration.airbus.AirbusProducer;
import com.myntra.migration.snapshortmigration.db.entity.UserEntity;
import com.myntra.migration.snapshortmigration.db.repo.UserEntityRepo;
import com.myntra.migration.snapshortmigration.domain.PublishData;
import com.myntra.migration.snapshortmigration.domain.StatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author Abhishyam.c on 02/11/20
 */
@RestController
@Slf4j
public class MainController {
    @Autowired
    private UserEntityRepo userEntityRepo;
    @Autowired
    private AirbusProducer airbusProducer;
    @Value("${airbus.eventName}")
    private String eventName;

    @Autowired
    private FileUtil fileUtil;

    @PostMapping("publishUidx")
    public Flux<UserEntity> publishUserInformation(@RequestBody PublishData publishData){
        Flux<UserEntity> userEntityFlux;
        if(publishData.isMigrateAll())
            userEntityFlux = userEntityRepo.findAll();
        else
            userEntityFlux= userEntityRepo.findByIdGreaterThanEqualAndIdLessThanEqualAndAppIdEquals(publishData.getStart(), publishData.getEnd(), 1L);

        userEntityFlux.doOnNext(userEntity -> {
            JSONObject userUidx = new JSONObject();
            userUidx.put("uidx", userEntity.getUidx());
            airbusProducer.publishToAirbus(eventName, userUidx);
        }).log().subscribe();
        return userEntityFlux;
    }

    @PostMapping("publishFromCSV")
    public void publishFromCSV(@RequestBody PublishData publishData) throws IOException, URISyntaxException {
      //  fileUtil.loadFiles();
        int i =0;
        try(BufferedReader br = new BufferedReader(new FileReader(publishData.getFilePath()))){
            String line;
            while ((line = br.readLine()) != null && i <= publishData.getEnd()) {
                if(i < publishData.getStart()) {
                    i++;
                    continue;
                }
                line = trimLine(line);
                JSONObject userUidx = new JSONObject();
                userUidx.put("uidx", line);
                userUidx.put("clientId", publishData.getClientId());
                userUidx.put("tenantId", publishData.getTenantId());
                airbusProducer.publishToAirbus(publishData.getEventName(), userUidx);
                if(i% publishData.getRecordsCountToPause() == 0) {
                    Thread.sleep(publishData.getSleep());
                }
                log.info("Published UIDX: {}", line);
                if(i%100 == 0){
                    log.info("Completed Till {}", i);
                }
                i++;
            }
       //     fileUtil.close();
        } catch (IOException  | InterruptedException e) {
            log.error("Error reading data from csv file {}", publishData.getFilePath(), e);
        //    fileUtil.close();
            throw new IOException(e);
        }
    }

    public static String trimLine(String line) {
        line = line.replaceAll("\"", "");
        line = line.trim();
        return line;
    }

    @PostMapping("uidx")
    public ResponseEntity<StatusResponse> publishUserInformationList(@RequestBody PublishData publishData){
        JSONObject userUidx = new JSONObject();
        userUidx.put("uidx", publishData.getUidx());
        return ResponseEntity.ok(airbusProducer.publishToAirbus(eventName, userUidx));
    }
}
