package com.myntra.migration.snapshortmigration.controllers;

import com.myntra.migration.snapshortmigration.db.entity.UserEmailLookupArchive;
import com.myntra.migration.snapshortmigration.db.entity.UserEntity;
import com.myntra.migration.snapshortmigration.db.entity.UserPhoneLookupArchive;
import com.myntra.migration.snapshortmigration.db.entity.UserSocialLinkLookupArchive;
import com.myntra.migration.snapshortmigration.domain.DeleteUser;
import com.myntra.migration.snapshortmigration.domain.PublishData;
import com.myntra.migration.snapshortmigration.service.TenantDbMigrationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.List;

/**
 * @author Abhishyam.c on 05/11/20
 */
@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/tenants")
public class TenantDbMigrationController {

    private final TenantDbMigrationService tenantDbMigrationService;

    @PostMapping("/migrateEmails")
    public Flux<UserEmailLookupArchive> migrateEmails(@RequestBody PublishData publishData){
        log.info("Migrating the user_email_lookup_archive table from 1.0 to user_email_lookup_archive table in 2.0");
        return tenantDbMigrationService.migrateEmailsTable(publishData);
    }

    @PostMapping("/migratePhone")
    public Flux<UserPhoneLookupArchive> migratePhone(@RequestBody PublishData publishData){
        log.info("Migrating the user_phone_lookup_archive table from 1.0 to user_phone_lookup_archive table in 2.0");
        return tenantDbMigrationService.migratePhoneTable(publishData);
    }

    @PostMapping("/migrateSocialLinks")
    public Flux<UserSocialLinkLookupArchive> migrateSocialLinks(@RequestBody PublishData publishData){
        log.info("Migrating the user_email_lookup_archive table from 1.0 to user_email_lookup_archive table in 2.0");
        return tenantDbMigrationService.migrateSocialLinksTable(publishData);
    }

    @PostMapping("/deleteUsers")
    public void deleteUsers(@RequestBody DeleteUser deleteUser){
        log.info("Deleting user from Account service inside DB");
         tenantDbMigrationService.deleteUser(deleteUser);
    }

    @PostMapping("/getUsers")
    public Flux<UserEntity> getUsers(@RequestBody List<String> usersEmailList){
        log.info("Get User Uidx from Email");
        return tenantDbMigrationService.getUserFromEmail(usersEmailList);
    }

    @PostMapping("/deleteSocialLinks")
    public void updateUserInSocialLookup( @RequestBody PublishData publishData){
        log.info("Deleting the users in user_social_link_lookup in file: {}",publishData.getFilePath());
        tenantDbMigrationService.deleteSocialLink(publishData);
    }
}
