package com.myntra.migration.snapshortmigration.controllers;

import com.myntra.migration.snapshortmigration.db.accountmodels.Clients;
import com.myntra.migration.snapshortmigration.db.entity.AppClient;
import com.myntra.migration.snapshortmigration.db.entity.AppClientKey;
import com.myntra.migration.snapshortmigration.db.entity.PasswordPolicy;
import com.myntra.migration.snapshortmigration.domain.ClientUpdateRequest;
import com.myntra.migration.snapshortmigration.domain.TenantUpdateRequest;
import com.myntra.migration.snapshortmigration.service.MasterDBMigrationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * @author Abhishyam.c on 04/11/20
 */
@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/master")
public class MasterDBMigrationController {
    private final MasterDBMigrationService masterDBMigrationService;

    @PostMapping("/migrateAppTable")
    public void migrateAppTable(){
        log.info("Migrating the Apps table from 1.0 to tenants table in 2.0");
        masterDBMigrationService.migrateAppTable();
    }

    @PostMapping("/migratePasswordPolicyTable")
    public Flux<PasswordPolicy> migratePasswordPolicyTable(){
        log.info("Migrating the PasswordPolicy table from 1.0 to PasswordPolicy table in 2.0");
        return masterDBMigrationService.migratePasswordPolicyTable();
    }

    @PostMapping("/migrateClientsTable")
    public Flux<AppClient> migrateClientsTable(){
        log.info("Migrating the Clients table from 1.0 to Clients table in 2.0");
        return masterDBMigrationService.migrateClientsTable();
    }

    @PostMapping("/migrateClientPropertiesTable")
    public Flux<AppClient> migrateClientPropertiesTable(){
        log.info("Migrating the Clients  table from 1.0 to Clients Properties table in 2.0");
        return masterDBMigrationService.migrateClientPropertiesTable();
    }

    @PostMapping("/migrateClientKeysTable")
    public Flux<AppClientKey> migrateClientKeysTable(){
        log.info("Migrating the Clients Keys table from 1.0 to Clients keys table in 2.0");
        return masterDBMigrationService.migrateClientKeysTable();
    }

    @PostMapping("/migrateClient")
    public void migrateClientsTable(@RequestBody ClientUpdateRequest clientRequest){
        log.info("Migrating the Client {} table from 1.0 to Clients table in 2.0", clientRequest.getClientId());
         masterDBMigrationService.migrateClient(clientRequest);
    }

    @DeleteMapping("/deletePasswordPolicyTable")
    public Mono<Integer> deletePasswordPolicyTable(){
        log.info("Truncating the PasswordPolicy table from 2.0");
        return masterDBMigrationService.truncatePasswordPolicyTable();
    }

    @DeleteMapping("/deleteTenantsTable")
    public Mono<Integer> deleteTenantsTable(){
        log.info("Truncating the Tenants table from 2.0");
        return masterDBMigrationService.truncateTenantsTable();
    }

    @DeleteMapping("/deleteClientsTable")
    public Mono<Integer> deleteClientsTable(){
        log.info("Truncating the Clients table from 2.0");
        return masterDBMigrationService.truncateClientsTable();
    }

    @DeleteMapping("/deleteClientPropertiesTable")
    public Mono<Integer> deleteClientPropertiesTable(){
        log.info("Truncating the Client_properties table from 2.0");
        return masterDBMigrationService.truncateClientPropertiesTable();
    }

    @DeleteMapping("/deleteClientKeysTable")
    public Mono<Integer> deleteClientKeysTable(){
        log.info("Truncating the Client_keys table from 2.0");
        return masterDBMigrationService.truncateClientKeysTable();
    }

    @DeleteMapping("/deleteClientKeys")
    public void deleteClientKeysTable(@RequestBody Integer clientId){
        log.info("Truncating the Client_keys table from 2.0");
         masterDBMigrationService.truncateClientKeys(clientId);
    }

    @PostMapping("/deleteClientsTable")
    public void deleteClientsTable(@RequestBody List<String> clientsToDelete){
        log.info("Deleting the Clients table from 2.0");
        masterDBMigrationService.deleteFromClients(clientsToDelete);
    }

    @PostMapping("/migrateAllMasterTables")
    public ResponseEntity.BodyBuilder migrateAllMasterTable(){
        log.info("Migrating the All Master table from 1.0 to in 2.0");
        log.info("1.0 Table \t\t\t\t 2.0 Table");
        log.info("----------------------------------------------------------------------------");
        log.info("1.  password_policy  ------> Password policy");
        log.info("2. apps ------------------------> tenants");
        log.info("3. clients ----------------------> clients");
         masterDBMigrationService.migrateAllTable();
         return ResponseEntity.ok();
    }

    @PutMapping("updateClient")
    public void updateClient(@RequestBody ClientUpdateRequest clientUpdateRequest){
            log.info("Updating the client with id:{}", clientUpdateRequest.getClientId());
             masterDBMigrationService.updateClient(clientUpdateRequest);
    }

    @PutMapping("updateTenant")
    public void updateTenant(@RequestBody TenantUpdateRequest tenantUpdateRequest){
        log.info("Updating the tenant with id:{}", tenantUpdateRequest.getTenantId());
        masterDBMigrationService.updateTenant(tenantUpdateRequest);
    }

    @GetMapping("/accountServiceClients")
    public Flux<Clients> getClientsFromAccountService(){
            return masterDBMigrationService.getAllClients();
    }

}















