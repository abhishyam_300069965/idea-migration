package com.myntra.migration.snapshortmigration.controllers;

import com.myntra.migration.snapshortmigration.domain.PublishData;
import com.myntra.migration.snapshortmigration.service.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/validate")
public class ValidateController {

    @Autowired
    private ValidateService validateService;

    @PostMapping("/emailPattern")
    public void validateEmail(@RequestBody PublishData publishData) {
        validateService.validateEmail(publishData);
    }
}
