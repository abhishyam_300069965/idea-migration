package com.myntra.migration.snapshortmigration.service;

import com.myntra.migration.snapshortmigration.FileUtil;
import com.myntra.migration.snapshortmigration.constants.Transformation;
import com.myntra.migration.snapshortmigration.db.accountmodels.*;
import com.myntra.migration.snapshortmigration.db.connections.InsideDbConnection;
import com.myntra.migration.snapshortmigration.db.connections.MyntraDbConnection;
import com.myntra.migration.snapshortmigration.db.entity.*;
import com.myntra.migration.snapshortmigration.db.repo.*;
import com.myntra.migration.snapshortmigration.domain.DeleteUser;
import com.myntra.migration.snapshortmigration.domain.PublishData;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.relational.core.query.Update;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.myntra.migration.snapshortmigration.constants.FunctionalInterfaceConstants.*;
import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

/**
 * @author Abhishyam.c on 05/11/20
 */
@Service
@Slf4j
@AllArgsConstructor
public class TenantDbMigrationService {
    private final MyntraDbConnection myntraDbConnection;
    private final InsideDbConnection insideDbConnection;
    private final UserEmailLookupArchiveRepo userEmailLookupArchiveRepo;
    private final UserPhoneLookupArchiveRepo userPhoneLookupArchiveRepo;
    private final UserSocialLinkLookupArchiveRepo userSocialLinkLookupArchiveRepo;
    private final Transformation transformation;
    private UserEntityRepo userEntityRepo;
    private UserEmailRepo userEmailRepo;
    private final UserEmailLookupRepo userEmailLookupRepo;
    private UserSocialLinkLookupRepo userSocialLinkLookupRepo;
    private final FileUtil fileUtil;

    public Flux<UserEmailLookupArchive> migrateEmailsTable(PublishData publishData) {
        Flux<UserEmailLookupArchive> userEmailLookupArchiveFlux;
        if(publishData.isMigrateAll())
            userEmailLookupArchiveFlux = userEmailLookupArchiveRepo.findAll();
        else
            userEmailLookupArchiveFlux = userEmailLookupArchiveRepo
                    .findByIdGreaterThanEqualAndIdLessThanEqual(publishData.getStart(), publishData.getEnd());

        userEmailLookupArchiveFlux
                .filter(checkIfEmailMyntraAppId)
                .map(transformation::transformEmailArchive)
                .subscribe(
                        userEmailArchive ->
                            myntraDbConnection.getR2dbcEntityTemplate()
                                    .insert(UserEmailArchive.class)
                                    .using(userEmailArchive)
                        .log()
                        .subscribe()
                );
        userEmailLookupArchiveFlux
                .filter(checkIfEmailMyntraAppId.negate())
                .map(transformation::transformEmailArchive)
                .subscribe(userEmailArchive -> insideDbConnection.getR2dbcEntityTemplate()
                        .insert(UserEmailArchive.class)
                        .using(userEmailArchive)
                        .log()
                        .subscribe()
                );
        return userEmailLookupArchiveFlux;
    }

    public Flux<UserPhoneLookupArchive> migratePhoneTable(PublishData publishData) {
        Flux<UserPhoneLookupArchive> userPhoneLookupArchiveFlux;
        if(publishData.isMigrateAll())
            userPhoneLookupArchiveFlux= userPhoneLookupArchiveRepo.findAll();
        else
            userPhoneLookupArchiveFlux = userPhoneLookupArchiveRepo
                    .findByIdGreaterThanEqualAndIdLessThanEqual(publishData.getStart(), publishData.getEnd());
        userPhoneLookupArchiveFlux.filter(checkIfPhoneMyntraAppId)
                .map(transformation::transformPhoneArchive)
                .subscribe(userPhoneArchive -> myntraDbConnection.getR2dbcEntityTemplate()
                        .insert(UserPhoneArchive.class)
                        .using(userPhoneArchive)
                        .log()
                        .subscribe());

        userPhoneLookupArchiveFlux.filter(checkIfPhoneMyntraAppId.negate())
                .map(transformation::transformPhoneArchive)
                .subscribe(userPhoneArchive -> insideDbConnection.getR2dbcEntityTemplate()
                        .insert(UserPhoneArchive.class)
                        .using(userPhoneArchive)
                        .log()
                        .subscribe());
        return userPhoneLookupArchiveFlux;
    }

    public Flux<UserSocialLinkLookupArchive> migrateSocialLinksTable(PublishData publishData) {
        Flux<UserSocialLinkLookupArchive> userSocialLinkLookupArchiveFlux;
        if(publishData.isMigrateAll())
            userSocialLinkLookupArchiveFlux = userSocialLinkLookupArchiveRepo.findAll();
        else
            userSocialLinkLookupArchiveFlux = userSocialLinkLookupArchiveRepo
                    .findByIdGreaterThanEqualAndIdLessThanEqual(publishData.getStart(), publishData.getEnd());
        userSocialLinkLookupArchiveFlux.filter(checkIfSocialMyntraAppId)
                .map(transformation::transformSocialLinksArchive)
                .subscribe(userSocialLinkArchive -> myntraDbConnection.getR2dbcEntityTemplate()
                        .insert(UserSocialLinkArchive.class)
                        .using(userSocialLinkArchive)
                        .log()
                        .subscribe());

        userSocialLinkLookupArchiveFlux.filter(checkIfSocialMyntraAppId.negate())
                .map(transformation::transformSocialLinksArchive)
                .subscribe(userSocialLinkArchive -> insideDbConnection.getR2dbcEntityTemplate()
                        .insert(UserSocialLinkArchive.class)
                        .using(userSocialLinkArchive)
                        .log()
                        .subscribe());
        return userSocialLinkLookupArchiveFlux;
    }

    public void deleteRecoveryRequest(PublishData publishData) {
            switch (publishData.getSchema()){
                case "myntra": deleteInMyntra(publishData);
                break;
                case "inside": deleteInInside(publishData);
                break;
                default: log.info("Unknown dbName. It  should be myntra or inside");
        }
    }

    private void deleteInInside(PublishData publishData) {
        insideDbConnection.getR2dbcEntityTemplate()
                .delete(UserRecoveryRequest.class)
                .from("user_recovery_request")
                .all()
                .log()
                .subscribe();
    }

    private void deleteInMyntra(PublishData queriesToDelete) {
        myntraDbConnection.getR2dbcEntityTemplate()
                .delete(UserRecoveryRequest.class)
                .from("user_recovery_request")
                .all()
                .log()
        .subscribe();
    }

    public void updateUser(PublishData publishData) {
        switch (publishData.getSchema()){
            case "myntra": updateMyntraUser(publishData);
                break;
            case "inside": updateInsideUser(publishData);
                break;
            default: log.info("Unknown dbName. It  should be myntra or inside");
        }
    }

    private void updateInsideUser(PublishData publishData) {
        if(publishData.getUsers() != null && !publishData.getUsers().isEmpty()){
            for (String uidx : publishData.getUsers()) {
                updateUser(uidx, publishData);
            }
        }else {
            updateUser(publishData.getUidx(), publishData);
        }
    }

    private void updateUser(String uidx, PublishData publishData) {
        insideDbConnection.getR2dbcEntityTemplate()
                .update(User.class)
                .inTable("user")
                .matching(query(where("uidx").is(uidx)))
                .apply(Update.update(publishData.getColumnName(), publishData.getColumnValue()))
                .log()
                .subscribe();
    }

    private void updateMyntraUser(PublishData publishData) {
        myntraDbConnection.getR2dbcEntityTemplate()
                .update(User.class)
                .inTable("user")
                .matching(query(where("uidx").is(publishData.getUidx())))
                .apply(Update.update(publishData.getColumnName(), publishData.getColumnValue()))
        .log()
        .subscribe();
    }

    public Map<Long, String> getDifferences(PublishData publishData) throws IOException, URISyntaxException {
        fileUtil.loadFiles();
        Map<Long, String> result = new HashMap<>();
        //from idea get all id's and uidx.
        //keep then in map
        Mono<Map<Long, String>> usersDataFromIdea = getUsersDataFromIdea(publishData);
        usersDataFromIdea.doOnNext(longStringMap -> log.info("Total records from Idea: {}", longStringMap.size())).subscribe();
        // from account service get all id's
        Mono<Set<Long>> userIdsFromAccountService = getUsersDataFromAccountService(publishData);

        userIdsFromAccountService.doOnNext(longs -> log.info("Total records from Account service: {}", longs.size())
        ).subscribe(longs -> usersDataFromIdea.doOnNext(ideaUsersData -> {
            for (Map.Entry<Long, String> entry : ideaUsersData.entrySet()) {
                if(!longs.contains(entry.getKey())){
                    result.put(entry.getKey(), entry.getValue());
                    log.info(entry.getKey()+"---"+entry.getValue());
                    fileUtil.writeToFailureFile("\"");
                    fileUtil.writeToFailureFile(entry.getValue());
                    fileUtil.writeToFailureFile("\"");
                    fileUtil.writeToFailureFile("\n");
                }
            }
        }).subscribe(longStringMap -> {
            try {
                fileUtil.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
        //check each id's is there in list

        //if yes ignore, if not return to result
        log.info("printing the result");
        return result;

    }

    public void getDifferencesReverse(PublishData publishData) throws IOException {
        fileUtil.loadFiles();
        Mono<Set<String>> userIdsFromAccountService = getUidxFromAccountService(publishData);
        Mono<Set<String>> usersDataFromIdea = getUidxFromIdea(publishData);
        usersDataFromIdea.doOnNext(map -> log.info("Total records from Idea: {}", map.size())).subscribe();
        userIdsFromAccountService.doOnNext(longs -> log.info("Total records from Account service: {}", longs.size()))
                .subscribe(longs -> usersDataFromIdea.doOnNext(map -> longs.forEach(id -> {
                    if(!map.contains(id)){
                        log.info(String.valueOf(id));
                        fileUtil.writeToFailureFile("\"");
                        fileUtil.writeToFailureFile(id);
                        fileUtil.writeToFailureFile("\"");
                        fileUtil.writeToFailureFile("\n");
                    }
                })).subscribe(longStringMap -> {
                            try {
                                fileUtil.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        })
                );
    }

    private Mono<Set<String>> getUidxFromAccountService(PublishData publishData) {
        Criteria fetchUserIdsInRange = where("id").between(publishData.getStart(), publishData.getEnd());
        return myntraDbConnection.getR2dbcEntityTemplate()
                .select(query(fetchUserIdsInRange), User.class)
                .map(User::getUidx)
                .collect(Collectors.toSet());
    }


    private Mono<Set<Long>> getUsersDataFromAccountService(PublishData publishData) {
        Criteria fetchUserIdsInRange = where("id").between(publishData.getStart(), publishData.getEnd());
         return myntraDbConnection.getR2dbcEntityTemplate()
                .select(query(fetchUserIdsInRange), User.class)
                .map(user -> user.getId().longValue())
                .collect(Collectors.toSet());
    }

    private Mono<Set<String>> getUidxFromIdea(PublishData publishData) {
        Flux<UserEntity> userEntityFlux= userEntityRepo.
                findByIdGreaterThanEqualAndIdLessThanEqualAndAppIdEquals(publishData.getStart(), publishData.getEnd(), 1L);
        return userEntityFlux.map(UserEntity::getUidx).collect(Collectors.toSet());
    }

    private Mono<Map<Long, String>> getUsersDataFromIdea(PublishData publishData) {
        Flux<UserEntity> userEntityFlux= userEntityRepo.
                findByIdGreaterThanEqualAndIdLessThanEqualAndAppIdEquals(publishData.getStart(), publishData.getEnd(), 1L);
        return userEntityFlux.collect(Collectors.toMap(UserEntity::getId, UserEntity::getUidx));
    }

    public void deleteUser(DeleteUser deleteUser) {
        switch (deleteUser.getTenant()){
            case "myntra": deleteMyntraUser(deleteUser.getIds());
                break;
            case "inside": deleteInsideUser(deleteUser.getIds());
                break;
            default: log.info("Unknown dbName. It  should be myntra or inside");
        }
    }

    private void deleteInsideUser(List<BigDecimal> ids) {
        if(ids == null || ids.size() == 0){
            log.info("No records to delete");
            return;
        }
        for (BigDecimal id : ids) {
            log.info("Deleting user with Id:{}", id);
            deleteImageEntries(insideDbConnection, id);
            deleteSocialLinks(insideDbConnection, id);
            deleteCredentials(insideDbConnection, id);
            deleteUser(insideDbConnection, id);
        }
    }

    private void deleteUser(InsideDbConnection insideDbConnection, BigDecimal id) {
        insideDbConnection.getR2dbcEntityTemplate()
                .delete(User.class)
                .from("user")
                .matching(query(where("id").is(id))).all()
                .subscribe();
    }

    private void deleteCredentials(InsideDbConnection insideDbConnection, BigDecimal id) {
        insideDbConnection.getR2dbcEntityTemplate()
                .delete(UserCredentials.class)
                .from("user_credentials")
                .matching(query(where("uid").is(id))).all()
                .subscribe();
    }

    private void deleteSocialLinks(InsideDbConnection insideDbConnection, BigDecimal id) {
        insideDbConnection.getR2dbcEntityTemplate()
                .delete(SocialLinks.class)
                .from("social_links")
                .matching(query(where("uid").is(id))).all()
                .subscribe();
    }

    private void deleteImageEntries(InsideDbConnection insideDbConnection, BigDecimal id) {
        insideDbConnection.getR2dbcEntityTemplate()
                .delete(ImagesEntries.class)
                .from("image_entries")
                .matching(query(where("uid").is(id))).all()
                .subscribe();
    }

    private void deleteMyntraUser(List<BigDecimal> ids) {
    }

    public Flux<UserEntity> getUserFromEmail(List<String> usersEmailList) {
        Flux<UserEmailLookup> userEmailDetailFlux = userEmailLookupRepo.findAllByEmailIn(usersEmailList).log();
        Flux<Long> uidFlux = userEmailDetailFlux.map(UserEmailLookup::getUid).log();
        return userEntityRepo.findAllById(uidFlux);
    }

    public void updateSocialLinks(PublishData publishData) {
        insideDbConnection.getR2dbcEntityTemplate()
                .update(SocialLinks.class)
                .inTable("social_links")
                .matching(query(where("id").is(publishData.getId())))
                .apply(Update.update(publishData.getColumnName(), publishData.getColumnValue()))
                .log()
                .subscribe();
    }

    public void deleteSocialLink(PublishData publishData){
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(publishData.getFilePath()))) {
            String line;
            while ((line = bufferedReader.readLine()) != null){
                String email = line.trim();
                deleteEntry(email);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteEntry(String email) {
        log.info("Deleting user for email: "+ email);
        Flux<Long> userEntryFlux = getUserEntityAccountService(email);
        userEntryFlux.doOnNext(id -> insideDbConnection.getR2dbcEntityTemplate()
                .delete(SocialLinks.class)
                .from("social_links")
                .matching(query(where("uid").is(id)
                        .and("type").is(0)))
                .all()
                .log()
                .subscribe()).log().subscribe();
    }

    private Flux<Long> getUserEntityAccountService(String email) {
        Criteria fetchUserByEmail = where("email").is(email);
        return insideDbConnection.getR2dbcEntityTemplate()
                .select(query(fetchUserByEmail), User.class)
                .map(user -> user.getId().longValue());
    }
}