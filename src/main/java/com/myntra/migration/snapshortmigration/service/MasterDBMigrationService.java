package com.myntra.migration.snapshortmigration.service;

import com.myntra.migration.snapshortmigration.constants.Transformation;
import com.myntra.migration.snapshortmigration.db.accountmodels.ClientKeys;
import com.myntra.migration.snapshortmigration.db.accountmodels.Clients;
import com.myntra.migration.snapshortmigration.db.accountmodels.ClientsProperties;
import com.myntra.migration.snapshortmigration.db.accountmodels.Tenants;
import com.myntra.migration.snapshortmigration.db.connections.MasterDbConnection;
import com.myntra.migration.snapshortmigration.db.entity.*;
import com.myntra.migration.snapshortmigration.db.repo.*;
import com.myntra.migration.snapshortmigration.domain.ClientUpdateRequest;
import com.myntra.migration.snapshortmigration.domain.TenantUpdateRequest;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.relational.core.query.Query;
import org.springframework.data.relational.core.query.Update;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

/**
 * @author Abhishyam.c on 04/11/20
 */
@Service
@Slf4j
@AllArgsConstructor
public class MasterDBMigrationService {
    private final AppRepo appRepo;
    private final PasswordPolicyRepo passwordPolicyRepo;
    private final AppClientRepo appClientRepo;
    private final AppClientKeysRepo appClientKeysRepo;
    private final AppPasswordPolicyRepo appPasswordPolicyRepo;
    private final MasterDbConnection masterDbConnection;
    private final Transformation transformation;

    public void migrateAppTable() {
        Map<Long, Long> passwordPolicyCache = new HashMap<>();
        appPasswordPolicyRepo.findAll().collectMap(AppPasswordPolicy::getAppid,
                AppPasswordPolicy::getPasswordPolicyid).subscribe(passwordPolicyCache::putAll);
        Flux<App> appFlux = appRepo.findAll();
        appFlux.map(app -> transformation.transformAppToTenant(app, passwordPolicyCache))
                .log()
                .subscribe(tenants -> masterDbConnection.getR2dbcEntityTemplate()
                        .insert(Tenants.class)
                        .using(tenants)
                        .log("inserting row into tenant table with Id: "+ tenants.getId())
                        .subscribe());
    }

    public Flux<PasswordPolicy> migratePasswordPolicyTable() {
        Flux<PasswordPolicy> passwordPolicyFlux = passwordPolicyRepo.findAll();
        passwordPolicyFlux.map(transformation::transformPasswordPolicy)
                .log()
                .subscribe(passwordPolicy -> masterDbConnection.getR2dbcEntityTemplate()
                .insert(com.myntra.migration.snapshortmigration.db.accountmodels.PasswordPolicy.class)
                .using(passwordPolicy)
                .log("inserting row into passwordpolicy table with id: "+ passwordPolicy.getId())
                .subscribe());
        return passwordPolicyFlux;
    }


    public Flux<AppClient> migrateClientsTable() {
        Flux<AppClient> appClientFlux = appClientRepo.findAll();
        appClientFlux.map(appClient -> transformation.transformAppClientsTOClients(appClient, null))
                .subscribe(clients -> masterDbConnection.getR2dbcEntityTemplate()
                        .insert(Clients.class)
                        .using(clients)
                        .log()
                        .subscribe()
                );
        return appClientFlux;
    }

    public Flux<AppClient> migrateClientPropertiesTable() {
        Flux<AppClient> appClientFlux = appClientRepo.findAll();

        appClientFlux.map(transformation::transformAppClientsTOClientProperties)
                .subscribe(clientsProperties -> masterDbConnection.getR2dbcEntityTemplate()
                        .insert(ClientsProperties.class)
                        .using(clientsProperties)
                        .log()
                        .subscribe()
                );
        return appClientFlux;
    }

    public Flux<AppClientKey> migrateClientKeysTable() {
        Flux<AppClientKey> appClientKeyFlux = appClientKeysRepo.findAll();
        appClientKeyFlux.map(transformation::transformAppClientKeysToClientKeys)
                    .subscribe(clientKeys -> masterDbConnection.getR2dbcEntityTemplate().insert(ClientKeys.class)
                    .using(clientKeys)
                    .log()
                    .subscribe()
                );
        return appClientKeyFlux;
    }

    public void migrateAllTable() {
        migratePasswordPolicyTable();
        migrateAppTable();
        migrateClientsTable();
    }

    public Mono<Integer> truncatePasswordPolicyTable() {
        return masterDbConnection.getR2dbcEntityTemplate()
                .delete(PasswordPolicy.class)
                .from("password_policy")
                .all();
    }

    public Mono<Integer> truncateTenantsTable() {
        return masterDbConnection.getR2dbcEntityTemplate()
                .delete(Tenants.class)
                .from("tenants")
                .all();
    }

    public Mono<Integer> truncateClientsTable() {
        return masterDbConnection.getR2dbcEntityTemplate()
                .delete(Clients.class)
                .from("clients")
                .all();
    }

    public Mono<Integer> truncateClientPropertiesTable() {
        return masterDbConnection.getR2dbcEntityTemplate()
                .delete(ClientsProperties.class)
                .from("clients_properties")
                .all();
    }

    public Mono<Integer> truncateClientKeysTable() {
        return masterDbConnection.getR2dbcEntityTemplate()
                .delete(ClientKeys.class)
                .from("client_keys")
                .all();
    }

    public void updateClient(ClientUpdateRequest clientUpdateRequest) {
        masterDbConnection.getR2dbcEntityTemplate()
                .select(Clients.class)
                .all()
                .filter(clients -> clientUpdateRequest.getClientNames().contains(clients.getName()))
                .map(Clients::getId)
                .subscribe(clientId -> masterDbConnection.getR2dbcEntityTemplate()
                        .update(Clients.class)
                        .inTable("clients")
                        .matching(query(where("id").is(clientId)))
                        .apply(Update.update(clientUpdateRequest.getColumnName(), clientUpdateRequest.getColumnValues()))
                        .log()
                        .subscribe());
    }

    public void deleteFromClients(List<String> clientsToDelete) {
        Flux<Integer> clientsToDeleteFlux = masterDbConnection.getR2dbcEntityTemplate()
                .select(Clients.class)
                .all()
                .filter(clients -> clientsToDelete.contains(clients.getName()))
                .map(Clients::getId);

        clientsToDeleteFlux.subscribe(clientId -> masterDbConnection.getR2dbcEntityTemplate()
                .delete(Query.query(where("client_id").is(clientId)), ClientsProperties.class)
                .log()
                .subscribe());

        /*clientsToDeleteFlux.subscribe(clientId -> masterDbConnection.getR2dbcEntityTemplate()
                .delete(Query.query(where("client_id").is(clientId)), ClientKeys.class)
                .log()
                .subscribe());*/

        clientsToDeleteFlux.subscribe(clientId -> masterDbConnection.getR2dbcEntityTemplate()
                        .delete(Query.query(where("id").is(clientId)), Clients.class)
                        .log()
                        .subscribe());
    }

    public void truncateClientKeys(Integer clientId) {
        Flux<Integer> clientsToDeleteFlux = masterDbConnection.getR2dbcEntityTemplate()
                .select(Clients.class)
                .all()
                .filter(clients -> clientId.equals(clients.getId()))
                .map(Clients::getId);

        clientsToDeleteFlux.subscribe(clients-> masterDbConnection.getR2dbcEntityTemplate()
                .delete(Query.query(where("client_id").is(clientId)), ClientKeys.class)
                .log()
                .subscribe());
    }

    public void migrateClient(ClientUpdateRequest clientUpdateRequest) {
        Mono<AppClient> appClientFlux = appClientRepo.findById(Long.valueOf(clientUpdateRequest.getClientId()));
        appClientFlux.map(appClient -> transformation.transformAppClientsTOClients(appClient, clientUpdateRequest.getTenantId()))
                .subscribe(clients -> masterDbConnection.getR2dbcEntityTemplate()
                        .insert(Clients.class)
                        .using(clients)
                        .log()
                        .subscribe()
                );
            appClientFlux.map(transformation::transformAppClientsTOClientProperties)
                    .subscribe(clientsProperties -> masterDbConnection.getR2dbcEntityTemplate()
                            .insert(ClientsProperties.class)
                            .using(clientsProperties)
                            .log()
                            .subscribe()
                    );
        Flux<AppClientKey> appClientKeyFlux = appClientKeysRepo.findByClientid(Long.valueOf(clientUpdateRequest.getClientId()));
        appClientKeyFlux.map(transformation::transformAppClientKeysToClientKeys)
                .subscribe(clientKeys -> masterDbConnection.getR2dbcEntityTemplate().insert(ClientKeys.class)
                        .using(clientKeys)
                        .log()
                        .subscribe()
                );
    }

    public Flux<Clients> getAllClients() {
        return masterDbConnection.getR2dbcEntityTemplate()
                .select(Clients.class).all();
    }

    public void updateTenant(TenantUpdateRequest tenantUpdateRequest) {
        masterDbConnection.getR2dbcEntityTemplate()
                .select(Tenants.class)
                .all()
                .filter(tenant -> tenantUpdateRequest.getTenantId().equals(tenant.getId()))
                .map(Tenants::getId)
                .subscribe(tenantId -> masterDbConnection.getR2dbcEntityTemplate()
                        .update(Tenants.class)
                        .inTable("tenants")
                        .matching(query(where("id").is(tenantId)))
                        .apply(Update.update("store_id", tenantUpdateRequest.getPartnerId()))
                        .log()
                        .subscribe());
    }


}
