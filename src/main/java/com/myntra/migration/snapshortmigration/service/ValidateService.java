package com.myntra.migration.snapshortmigration.service;

import com.myntra.migration.snapshortmigration.FileUtil;
import com.myntra.migration.snapshortmigration.domain.PublishData;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

@AllArgsConstructor
@Slf4j
@Service
public class ValidateService {

    private static final String EMAIL_PATTERN = "^([A-Za-z0-9_+\\-]+[\\.]?)+@((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z0-9]{2,}$";
    private static final String EMAIL_PATTERN_2 = "^[\\w+-.]+(?:\\.[\\w+-]+)*@(?:[a-zA-Z0-9-]{1,25}+\\.)+[a-zA-Z]{2,25}$";
    private static final Pattern EMAIL_REGEX = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
    private static final Pattern EMAIL_REGEX_2 = Pattern.compile(EMAIL_PATTERN_2, Pattern.CASE_INSENSITIVE);

    private final FileUtil fileUtil;

    public void validateEmail(PublishData publishData) {
        try {
            fileUtil.loadFiles();
        }catch (Exception e){
            log.error("Exception occured: ", e);
            return;
        }
        log.info("Start time: " + System.currentTimeMillis());
        try(BufferedReader br = new BufferedReader(new FileReader(publishData.getFilePath()))){
            String email;
            while ((email = br.readLine()) != null) {
               boolean first = EMAIL_REGEX.matcher(email).find();
               boolean second = EMAIL_REGEX_2.matcher(email).find();

               if (first != second) {
                   log.info("Email not valid: " + email);
                   fileUtil.writeToFailureFile(email);
                   fileUtil.writeToFailureFile("\n");
               }
            }
        } catch (IOException e) {
            log.error("Error reading data from csv file {}", publishData.getFilePath(), e);
        }
        log.info("end time: " + System.currentTimeMillis());
    }
}
