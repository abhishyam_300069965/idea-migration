package com.myntra.migration.snapshortmigration.constants;

import com.myntra.migration.snapshortmigration.db.accountmodels.PasswordPolicy;
import com.myntra.migration.snapshortmigration.db.accountmodels.*;
import com.myntra.migration.snapshortmigration.db.entity.*;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;


/**
 * @author Abhishyam.c on 04/11/20
 */
@Component
public class Transformation {

    @Autowired
    private ModelMapper modelMapper;

    private Predicate<Integer> checkIFTrue = input -> {
      if(input == null) return false;
        return input != 0;
    };
    public  Tenants transformAppToTenant(App app,  Map<Long, Long> passwordPolicyCache) {
        Tenants tenants =new Tenants();
        tenants.setId(app.getId().intValue());
        tenants.setPassId(passwordPolicyCache.getOrDefault(app.getId(), 1L));
        tenants.setName(app.getName());
        tenants.setDomain(app.getDomain());
        tenants.setActive(app.getActive());
        tenants.setDescription(app.getDescription());
        tenants.setSsoDomain(app.getSsoDomain());
        tenants.setStoreId(app.getStoreId());
        if(app.getStartsOn() != null)
            tenants.setStartsOn(app.getStartsOn().getTime());
        if(app.getEndsOn() != null)
            tenants.setEndsOn(app.getEndsOn().getTime());
        tenants.setCreatedBy(app.getCreatedBy());
        if(app.getVersion() != null)
            tenants.setVersion(app.getVersion().intValue());
        if(app.getCreatedOn() != null)
            tenants.setCreatedOn(app.getCreatedOn().getTime());
        if(app.getLastModifiedOn() != null)
            tenants.setLastModifiedOn(app.getLastModifiedOn().getTime());
        tenants.setDatasourceName(getDataSourceName(app.getName()));
        return tenants;
    }

    public  PasswordPolicy transformPasswordPolicy(com.myntra.migration.snapshortmigration.db.entity.PasswordPolicy old){
        PasswordPolicy newObject = modelMapper.map(old, PasswordPolicy.class);
        newObject.setLastModifiedOn(old.getLastModifiedOn().getTime());
        newObject.setCreatedOn(old.getCreatedOn().getTime());
        return newObject;
    }

    public Clients transformAppClientsTOClients(AppClient appClient, Long tenantId){
        Clients clients = modelMapper.map(appClient, Clients.class);
        tenantId = tenantId == null ? 5L : tenantId;
        clients.setTenantId(tenantId);
        clients.setLoginOption(appClient.getLoginOptions());
        if(appClient.getRegistrationOn() != null) {
            clients.setRegistrationOn(appClient.getRegistrationOn().getTime());
        }
        if(appClient.getLastModifiedOn() != null)
            clients.setLastModifiedOn(appClient.getLastModifiedOn().getTime());
        if(appClient.getExpiryOn() != null)
            clients.setExpiryOn(appClient.getExpiryOn().getTime());
        if(appClient.getCreatedOn() != null)
           clients.setCreatedOn(appClient.getCreatedOn().getTime());
        clients.setIntent(0);
        return clients;
    }

    public ClientsProperties transformAppClientsTOClientProperties(AppClient appClient){
        ClientsProperties clientsProperties = modelMapper.map(appClient, ClientsProperties.class);
        clientsProperties.setClientId(Math.toIntExact(appClient.getId()));
        clientsProperties.setEncodeToken(appClient.getEncodeToken() ==1);
        clientsProperties.setIssueRt(checkIFTrue.test(appClient.getIssueRt()));
        clientsProperties.setRefreshTokenType(appClient.getRefreshTokenType());
        clientsProperties.setMaxConcurrentSessions(appClient.getMaxConcurrentSession());
        clientsProperties.setConcurrentLoginEnabled(appClient.getIsBlockingConcurrentLoginEnabled());
        String redirectUri = appClient.getRedirectUri();
        if(isNotEmpty(redirectUri) && redirectUri.length() > 255)
            clientsProperties.setRedirectUri(redirectUri.substring(0, 250));
        else
            clientsProperties.setRedirectUri(redirectUri);
        return clientsProperties;
    }

    public UserEmailArchive transformEmailArchive(UserEmailLookupArchive old){
        UserEmailArchive newObject = modelMapper.map(old, UserEmailArchive.class);
        if(old.getArchivedOn() != null) {
            LocalDateTime localDateTime = old.getArchivedOn().toLocalDateTime();
            newObject.setArchivedOn(localDateTime);
        }
        newObject.setLastModifiedOn(old.getLastModifiedOn().getTime());
        newObject.setCreatedOn(old.getCreatedOn().getTime());
        return newObject;
    }

    public UserPhoneArchive transformPhoneArchive(UserPhoneLookupArchive old){
        UserPhoneArchive newObject = modelMapper.map(old, UserPhoneArchive.class);
        if(old.getArchivedOn() != null) {
            LocalDateTime localDateTime = old.getArchivedOn().toLocalDateTime();
            newObject.setArchivedOn(localDateTime);
        }
        newObject.setLastModifiedOn(old.getLastModifiedOn().getTime());
        newObject.setCreatedOn(old.getCreatedOn().getTime());
        return newObject;
    }

    public UserSocialLinkArchive transformSocialLinksArchive(UserSocialLinkLookupArchive old){
        UserSocialLinkArchive newObject = modelMapper.map(old, UserSocialLinkArchive.class);
        if(old.getArchivedOn() != null) {
            LocalDateTime localDateTime = old.getArchivedOn().toLocalDateTime();
            newObject.setArchivedOn(localDateTime);
        }
        newObject.setLastModifiedOn(old.getLastModifiedOn().getTime());
        newObject.setCreatedOn(old.getCreatedOn().getTime());
        return newObject;
    }

    private static String getDataSourceName(String name) {
        if(name.equals("myntra"))return "tenant_myntra";
        return "tenant_inside";
    }

    public ClientKeys transformAppClientKeysToClientKeys(AppClientKey appClientKey){
        ClientKeys clientKeys = new ClientKeys();
        clientKeys.setId(appClientKey.getId().intValue());
        clientKeys.setClientId(appClientKey.getClientid().intValue());
        clientKeys.setKeyType(getKeyType.apply(appClientKey.getKeyType()));
        clientKeys.setKeyAlgo(getAlgoType.apply(appClientKey.getKeyAlgo()));
        clientKeys.setPrivateKey(appClientKey.getPriKey());
        clientKeys.setPublicKey(appClientKey.getPubKey());
        clientKeys.setActive(appClientKey.getActive() ==1);
        clientKeys.setStartsOn(appClientKey.getStartsOn().getTime());
        if(appClientKey.getEndsOn() != null)
            clientKeys.setEndsOn(appClientKey.getEndsOn().getTime());
        clientKeys.setKeyVersion(appClientKey.getKeyVersion());
        clientKeys.setLastModifiedOn(appClientKey.getLastModifiedOn().getTime());
        if(appClientKey.getCreatedOn() != null)
            clientKeys.setCreatedOn(appClientKey.getCreatedOn().getTime());
        clientKeys.setVersion(appClientKey.getVersion());
        return clientKeys;
    }

    Function<String, Integer> getKeyType = intput ->{
        if(intput.equals("LOGGED_IN")) return 0;
        else return 1;
    };

    Function<String, Integer> getAlgoType = intput ->{
        if(intput.equals("HS256")) return 0;
        else return 1;
    };
}