package com.myntra.migration.snapshortmigration.constants;

import com.myntra.migration.snapshortmigration.db.accountmodels.UserPhoneArchive;
import com.myntra.migration.snapshortmigration.db.accountmodels.UserSocialLinkArchive;
import com.myntra.migration.snapshortmigration.db.entity.UserEmailLookupArchive;
import com.myntra.migration.snapshortmigration.db.entity.UserPhoneLookupArchive;
import com.myntra.migration.snapshortmigration.db.entity.UserSocialLinkLookupArchive;

import java.util.function.Predicate;

/**
 * @author Abhishyam.c on 05/11/20
 */
public final class FunctionalInterfaceConstants {
    private FunctionalInterfaceConstants() {
    }

    public static final Predicate<UserEmailLookupArchive> checkIfEmailMyntraAppId = userEmailLookupArchive -> userEmailLookupArchive.getAppid().equals(1L);
    public static final Predicate<UserPhoneLookupArchive> checkIfPhoneMyntraAppId = userEmailLookupArchive -> userEmailLookupArchive.getAppid().equals(1L);
    public static final Predicate<UserSocialLinkLookupArchive> checkIfSocialMyntraAppId = userEmailLookupArchive -> userEmailLookupArchive.getAppid().equals(1L);
}
