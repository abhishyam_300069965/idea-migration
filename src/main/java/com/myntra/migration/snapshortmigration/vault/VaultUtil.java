package com.myntra.migration.snapshortmigration.vault;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Abhishyam.c on 19/11/20
 */
@Slf4j
public class VaultUtil {
    public static JSONObject getCredentials(String service, String credential) {
        BufferedReader input =null;
        Process process = null;

        try {
            String[] cmdArray =
                    {"/usr/local/bin/keycli_linux_amd64", "-service_name", service,
                            "-credential_name", credential};

            String out = null;
            process = Runtime.getRuntime().exec(cmdArray, null);
            input = new BufferedReader(new InputStreamReader(process.getInputStream()));

            out = input.readLine();
            input.close();
            JSONObject obj = new JSONObject(out);

            if (!obj.getBoolean("error")) {
                return obj;
            } else {
                throw new RuntimeException("Error fetching credentials from vault for account service");
            }

        } catch (Exception ex) {
            throw new RuntimeException("Error fetching credentials from vault for account service");
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    log.error("Error getting credentials from Vault, e");
                }
            }
            if(process!=null){
                process.destroy();
            }
        }
    }
}
