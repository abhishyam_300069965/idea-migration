package com.myntra.migration.snapshortmigration;

import com.myntra.airbus.shaded.google.common.io.Resources;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Abhishyam.c on 03/12/20
 */

@Slf4j
@Component
public class FileUtil {
    private  BufferedWriter successBufferWriter;
    private  BufferedWriter failureBufferWriter;

    public void loadFiles() throws IOException {
        successBufferWriter = new BufferedWriter(new FileWriter("/myntra/migration/success.txt"));
        failureBufferWriter = new BufferedWriter(new FileWriter("/myntra/migration/failed_users.csv"));

    }

    public void writeToSuccessFile(String uidx){
        try {
            successBufferWriter.write(uidx);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToFailureFile(String uidx){
        try {
            failureBufferWriter.write(uidx);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  void close() throws IOException {
        successBufferWriter.close();
        failureBufferWriter.close();
    }
}
