package com.myntra.migration.snapshortmigration.airbus;

import com.myntra.airbus.entry.EventEntry;
import com.myntra.airbus.exception.ManagerException;
import com.myntra.airbus.producer.Producer;
import com.myntra.airbus.shaded.google.common.io.Resources;
import com.myntra.migration.snapshortmigration.FileUtil;
import com.myntra.migration.snapshortmigration.domain.StatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Abhishyam.c on 03/11/20
 */
@Slf4j
@Component
public class AirbusProducer {

    @Autowired
    @Qualifier("producer")
    private Producer producer;

    @Value("${airbus.appName}")
    private String appName;

    @Autowired
    private FileUtil fileUtil;

    public StatusResponse publishToAirbus(String eventName, JSONObject data) {

        EventEntry eventEntry = new EventEntry();
        eventEntry.setAppName(appName);
        eventEntry.setEventName(eventName);
        eventEntry.setData(data.toString());
        log.info(data.toString());
        eventEntry.setPartitionKey(data.getString("uidx"));
        try {
            producer.send(eventEntry);
        //    fileUtil.writeToSuccessFile(data.getString("uidx"));
            return new StatusResponse(3, "Success");
        }catch (ManagerException e) {
            log.error("Error while sending message to airbus: ", e);
        //    fileUtil.writeToFailureFile(data.getString("uidx"));
            return new StatusResponse(51, e.getMessage());
        }
    }
}
