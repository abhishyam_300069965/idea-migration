package com.myntra.migration.snapshortmigration.airbus;

import com.myntra.airbus.exception.ManagerException;
import com.myntra.airbus.producer.Producer;
import com.myntra.airbus.producer.impl.ProducerImpl;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Abhishyam.c on 03/11/20
 */
@Slf4j
@Configuration
@Data
public class AirbusProducerConfig {

    @Value("${airbus.serviceURL}")
    private String serviceUrl;
    @Value("${airbus.appName}")
    private String appName;

    @Bean(name = "producer")
    public Producer producer() throws Exception {
        Producer producer;
        try {
            producer = new ProducerImpl(serviceUrl,appName);
        } catch (ManagerException e) {
            log.error("Error while creating producer: ", e);
            throw new Exception(e);
        }
        return producer;

    }

}
