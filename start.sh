processId=$(ps -ef | grep 'snapshort-migration-0.0.1-SNAPSHOT.jar' | grep -v 'grep' | awk '{ printf $2 }')
kill -9 $processId
export JAVA_HOME=/opt/java8
export PATH=$JAVA_HOME/bin:$PATH
./gradlew clean build -x test
JAR_PATH=build/libs
java  -jar $JAR_PATH/snapshort-migration-0.0.1-SNAPSHOT.jar &
